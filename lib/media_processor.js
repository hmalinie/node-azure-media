
module.exports = function(media) {

	var MediaProcessor = function(data) {
		for(var key in data) this[key] = data[key];
	};

	// static

	MediaProcessor.list = function(callback) {
		media.api.request('GET', media.api.service_url + '/MediaProcessors', function(err, res) {
			if(err) return callback(err);
			var list = [];
			for(var i = 0; i < res.d.results.length; i++) {
				list.push(new media.MediaProcessor(res.d.results[i]));
			}
			callback(null, list);
		});
	};

	MediaProcessor.getLatestByName = function(name, callback) {
		MediaProcessor.list(function(err, media_processors) {
			if(err) return callback(err);

			var list = [];
			for(var i = 0; i < media_processors.length; i++) {
				if(media_processors[i].Name === name) {
					list.push(media_processors[i]);
				}
			}

			list.sort(function(a, b) {
				return parseInt(b.Version, 10) - parseInt(a.Version, 10);
			});

			if(list.length) callback(null, list[0]);
			else callback('Not found Name: ' + name);
		});
	};

	return MediaProcessor;
};

