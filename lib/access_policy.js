
var async = require('async');

module.exports = function(media) {

	var AccessPolicy = function(data) {
		for(var key in data) this[key] = data[key];
	};
	
	AccessPolicy.Permissions = {
		None: 0,
		Read: 1,
		Write: 2,
		Delete: 4,
		List: 8
	};

	// static

	AccessPolicy.create = function(name, duration, permissions, callback) {
		media.api.request('POST', media.api.service_url + '/AccessPolicies', {
			Name: name,
			DurationInMinutes: duration,
			Permissions: permissions
		}, function(err, res) {
			if(err) return callback(err);
			callback(null, new AccessPolicy(res.d));
		});
	};

	AccessPolicy.refresh = function(id, callback) {
		media.api.request('GET', media.api.service_url + '/AccessPolicies(\'' + encodeURIComponent(id) + '\')', function(err, res) {
			if(err) return callback(err);
			callback(null, new Asset(res.d));
		});
	};

	AccessPolicy.deleteAll = function(callback) {
		media.api.request('GET', media.api.service_url + '/AccessPolicies', function(err, res) {
			if(err) return callback(err);
			async.forEachSeries(res.d.results, function(data, next) {
				new AccessPolicy(data).delete(next);	
			}, callback);
		});
	};

	// prototype

	AccessPolicy.prototype.refresh = function(callback) {
		var instance = this;
		media.api.request('GET', this.__metadata.uri, function(err, res) {
			if(err) return callback(err);
			for(var key in res.d) instance[key] = res.d[key];
			callback(null, instance);
		});
	};

	AccessPolicy.prototype.delete = function(callback) {
		media.api.request('DELETE', this.__metadata.uri, callback);
	};

	return AccessPolicy;
};

