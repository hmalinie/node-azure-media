
module.exports = function(media) {

	var Job = function(name, assets) {
		this.Name = name;
		this.InputMediaAssets = [];
		for(var i = 0; i < assets.length; i++) {
			this.InputMediaAssets.push({__metadata: {uri: assets[i].__metadata.uri}});
		}
		this.Tasks = [];
	};

	// static

	Job.get = function(id, callback) {
		media.api.request('GET', media.api.service_url + '/Jobs(\'' + encodeURIComponent(id) + '\')', function(err, res) {
			if(err) return callback(err);
			callback(null, new media.Asset(res));
		});
	};

	// prototype

	Job.prototype.addTask = function(name, media_processor_id, configuration, options) {
		var task = new media.Task(name, media_processor_id, configuration, options);
		this.Tasks.push(task);
		return task;
	};

	Job.prototype.refresh = function(callback) {
		media.api.request('GET', this.__metadata.uri, function(err, res) {
			if(err) return callback(err);
			for(var key in res.d) this[key] = res.d[key];
			callback(null, this);
		}.bind(this));
	};

	Job.prototype.getState = function(callback) {
		media.api.request('GET', this.__metadata.uri + '/State', function(err, res) {
			if(err) return callback(err);
			for(var key in res.d) this[key] = res.d[key];
			callback(null, this);
		}.bind(this));
	};

	Job.prototype.delete = function(callback) {
		media.api.request('DELETE', this.__metadata.uri, callback);
	};

	Job.prototype.submit = function(callback) {
		media.api.request('POST', media.api.service_url + '/Jobs', JSON.parse(JSON.stringify(this)), function(err, res) {
			if(err) return callback(err);
			for(var key in res.d) this[key] = res.d[key];
			callback(null, this);
		}.bind(this));
	};

	Job.prototype.cancel = function(callback) {
		media.api.request('GET', this.SERVICE_URL + '/CancelJob?jobid=\'' + encodeURIComponent(this.Id) + '\'', callback);
	};

	Job.prototype.readableState = function() {
		switch (this.State) {
			case 0: return 'Queued';
			case 1: return 'Scheduled';
			case 2: return 'Processing';
			case 3: return 'Finished';
			case 4: return 'Error';
			case 5: return 'Canceled';
			case 6: return 'Canceling';
			default: return 'Not readable';
		}
	};

	Job.prototype.getInputMediaAssets = function(callback) {
		media.api.request('GET', this.__metadata.uri + '/InputMediaAssets', function(err, res) {
			if(err) return callback(err);
			var assets = res.d.results;
			for(var i = 0; i < assets.length; i++) assets[i] = new Media.Asset(assets[i]);
			callback(null, assets);
		});
	};

	Job.prototype.getOutputMediaAssets = function(callback) {
		media.api.request('GET', this.__metadata.uri + '/OutputMediaAssets', function(err, res) {
			if(err) return callback(err);
			var assets = res.d.results;
			for(var i = 0; i < assets.length; i++) assets[i] = new media.Asset(assets[i]);
			callback(null, assets);
		});
	};

	return Job;
};