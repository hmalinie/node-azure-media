
var jsontoxml = require('jsontoxml');

module.exports = function(media) {

	var Task = function(name, media_processor_id, configuration, options) {
		this.Name = name;
		this.MediaProcessorId = media_processor_id;
		this.Configuration = configuration;
		this.Options = options;
		this.TaskBody = '<?xml version="1.0" encoding="utf-8"?>' + jsontoxml({
			taskBody: [
				{name:'inputAsset', text: 'JobInputAsset(0)'},
				{name:'outputAsset', text: 'JobOutputAsset(0)'}
			]
		});
	};

	return Task;
};