
module.exports = function(media) {

	var AssetFile = function(res) {
		
	};

	// static methods

	AssetFile.create = function(asset, callback) {
		media.api.request('GET', media.api.service_url + '/CreateFileInfos?assetid=\'' + encodeURIComponent(asset.Id) + '\'', function(err, res) {
			if(err) return callback(err);
			callback(null, new media.AssetFile(res));
		});
	};

	// prototype methods

	AssetFile.prototype.delete = function(callback) {
		media.api.request('DELETE', this.__metadata.uri, callback);
	};

	return AssetFile;
};