
var async = require('async');

module.exports = function(media) {

	var Asset = function(data) {
		for(var key in data) this[key] = data[key];
	};

	// static methods

	Asset.create = function(name, options, callback) {
		media.api.request('POST', media.api.service_url + '/Assets', { Name: name }, function(err, res) {
			if(err) return callback(err);
			callback(null, new Asset(res.d));
		});
	};

	Asset.get = function(id, callback) {
		media.api.request('GET', media.api.service_url + '/Assets(\'' + encodeURIComponent(id) + '\')', function(err, res) {
			if(err) return callback(err);
			callback(null, new Asset(res.d));
		});
	};

	// prototype methods

	Asset.prototype.refresh = function(callback) {
		media.api.request('GET', this.__metadata.uri, function(err, res) {
			if(err) return callback(err);
			
			for(var key in res.d) this[key] = res.d[key];
			callback(null, this);
		}.bind(this));
	};

	Asset.prototype.delete = function(callback) {
		media.api.request('DELETE', this.__metadata.uri, callback);
	};

	Asset.prototype.files = function(callback) {
		media.api.request('GET', this.Files.__deferred.uri, function(err, res) {
			if(err) return callback(err);

			this.Files.files = [];
			for(var i = 0; i < res.d.results.length; i++) this.Files.files.push(res.d.results[i]);
			callback(null, this.Files.files);
		}.bind(this));
	};

	Asset.prototype.deleteLocators = function(callback) {
		media.api.request('GET', this.__metadata.uri + '/Locators', function(err, res) {
			if(err) return callback(err);

			async.forEachSeries(res.d.results, function(data, next) {
				new media.Locator(data).delete(next);	
			}, callback);
		});
	};

	Asset.prototype.deleteAccessPolicies = function(callback) {
		media.api.request('GET', this.__metadata.uri + '/AccessPolicies', function(err, res) {
			if(err) return callback(err);

			async.forEachSeries(res.d.results, function(data, next) {
				new media.AccessPolicy(data).delete(next);
			}, callback);
		});
	};

	return Asset;
};
