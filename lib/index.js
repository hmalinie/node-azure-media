
var request = require('request');
var qs = require('querystring');

var API = require(__dirname + '/api.js');

var AzureMediaServices = module.exports = function(account_key, account_secret, callback) {
	
	AzureMediaServices.login(account_key, account_secret, function(err, api) {
		if(err) return callback(err);

		var media = {};
		media.api = api;

		media.Asset = require(__dirname + '/asset.js')(media);
		media.AssetFile = require(__dirname + '/asset_file.js')(media);
		media.Job = require(__dirname + '/job.js')(media);
		media.Task = require(__dirname + '/task.js')(media);
		media.Locator = require(__dirname + '/locator.js')(media);
		media.AccessPolicy = require(__dirname + '/access_policy.js')(media);
		media.MediaProcessor = require(__dirname + '/media_processor.js')(media);

		callback(null, media);
	}.bind(this));
};

AzureMediaServices.login = function(account_key, account_secret, callback) {
	request({
		method: 'POST',
		url: 'https://wamsprodglobal001acs.accesscontrol.windows.net/v2/OAuth2-13',
		strictSSL: true,
		json: true,
		body: qs.stringify({
			grant_type: 'client_credentials',
			client_id: account_key,
			client_secret: account_secret,
			scope: 'urn:WindowsAzureMediaServices'
		}, '&', '=')
	}, function(err, res, body) {
		if(!res) return callback(err);
		if(res.statusCode !== 200) return callback({statusCode: res.statusCode, error: body});
		callback(null, new API(body.access_token));
	}.bind(this));
};
