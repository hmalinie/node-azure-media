
var moment = require('moment');

module.exports = function(media) {

	var Locator = function(data) {
		for(var key in data) this[key] = data[key];
	};
	
	Locator.Type = {
		None: 0,
		SAS: 1,
		OnDemandOrigin: 2
	};

	// static methods

	Locator.create = function(name, type, asset, access_policy, callback) {
		media.api.request('POST', media.api.service_url + '/Locators', {
			Name: name,
			Type: type,
			AssetId: asset.Id,
			AccessPolicyId: access_policy.Id,
			StartTime: moment.utc().subtract('minutes', 5).format('M/D/YYYY h:mm:ss A')
		}, function(err, res) {
			if(err) return callback(err);
			
			callback(null, new media.Locator(res.d));
		});
	};

	// prototype methods

	Locator.prototype.get = function(callback) {
		var instance = this;
		media.api.request('GET', this.__metadata.uri, function(err, res) {
			if(err) return callback(err);
			for(var key in res.d) instance[key] = res.d[key];
			callback(null, instance);
		});
	};

	Locator.prototype.delete = function(callback) {
		media.api.request('DELETE', this.__metadata.uri, callback);
	};

	return Locator;
};

