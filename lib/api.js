
var request = require('request');
var fs = require('fs');
var moment = require('moment');
var _url = require('url');

var API = module.exports = function(access_token) {
	this.access_token = access_token;
	this.service_url = 'https://media.windows.net/API';
};

API.prototype.request = function(method, request_url, request_data, callback) {
	if(typeof callback === 'undefined') {
		callback = request_data;
		request_data = null;
	}

	var options = {
		url: request_url,
		method: method,
		followRedirect: false,
		headers: {
			'Accept': 'application/json;odata=verbose',
			'Content-Type': 'application/json;odata=verbose',
			'Content-Length': 0,
			'DataServiceVersion': '3.0',
			'MaxDataServiceVersion': '3.0',
			'x-ms-version': '2.2',
			'Authorization': 'Bearer ' + this.access_token
		}
	};

	if(request_data) {
		var data = JSON.stringify(request_data);
		options.headers['Content-Length'] = Buffer.byteLength(data);
		options.body = data;
	}
	
	//process.stdout.write('\n\n----------------------------------------\n');
	//process.stdout.write(method + ' ' + request_url + ' ... ');

	var req = request(options, function(err, res, body) {
		if(!res) return callback({error: err});

		//process.stdout.write(res.statusCode + '\n\n');

		if(res.statusCode === 301) {
			this.request(method, res.headers.location + _url.parse(request_url).path.substr(5), request_data, callback);
		} else {
			if(Math.floor(res.statusCode / 100) === 2) {
				try { body = JSON.parse(body); } catch(e) {}
				callback(null, body);
			} else {
				callback(this.createError(res));
			}
		}
	}.bind(this));
};

API.prototype.createError = function(res) {
	var err;
	try {
		var data = JSON.parse(res.body);
		err = {
			statusCode: res.statusCode,
			error: data.error.code,
			error_description: data.error.message.value
		};
	} catch(e) {
		err = {
			statusCode: res.statusCode,
			error: res.body
		};
	}
	return err;
}

API.prototype.uploadBlockBlob = function(upload_url, file_name, file_path, callback) {
	fs.stat(file_path, function(err, stat) {
		if(err) return callback(err);

		var x_ms_date = moment.utc().format('ddd, D MMM YYYY hh:mm:ss [GMT]');

		var headers = {
			'Content-Length': stat.size,
			'x-ms-blob-type': 'BlockBlob',
			'x-ms-date': x_ms_date,
			'x-ms-version': '2009-09-19',
			'x-ms-blob-content-disposition': 'attachment; filename="' + file_name + '"'
		};
		
		process.stdout.write('\n\n----------------------------------------\n');
		process.stdout.write('PUT ' + upload_url + ' ... ');
		
		var r = request({
			method: 'PUT',
			headers: headers,
			url: upload_url
		}, function(err, res, body) {
			if(!res) return callback({error: err});

			process.stdout.write(res.statusCode + '\n\n');

			if(Math.floor(res.statusCode / 100) === 2) callback();
			else callback(this.createError(res));
		}.bind(this));

		fs.createReadStream(file_path).pipe(r);
	}.bind(this));
};